# background_task.rb
#
# A class to modularize the background task workers

class BackgroundTask
  @@tasks = []

  def self.inherited(subclass)
    @@tasks << subclass unless @@tasks.include? subclass
  end

  def self.run_tasks
    @@tasks.each do |t|
      begin
        t.perform
      rescue StandardError => e
        say "Error running #{t}"
        puts e.message
        puts e.backtrace.join("\n")
      end
    end
  end
end

Dir[Rails.root.join('lib','background_tasks','*.rb').to_s].each {|file| require file }