# Example integrations

These are some example of integrating ISK to different data sources. They come from the production environment of Assembly computer festivals in Finland. They have been hacked together under some stress so the code isn't perfect but they have been proven to work.

The background tasks fetch schedule events from the main schedule backend and build several different schedules with filtering on the events.

We also fetch latest pictures from our official photographers and netcrew statistics to embed them into the slides as base64 encoded images.