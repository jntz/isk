# frozen_string_literal: true

class FetchJob < ApplicationJob
  queue_as :default

  def perform(slide)
    slide.fetch!
  end
end
